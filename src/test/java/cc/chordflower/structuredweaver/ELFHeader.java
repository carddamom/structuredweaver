/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver;

import cc.chordflower.structuredweaver.annotations.binary.BinaryModel;
import cc.chordflower.structuredweaver.annotations.binary.Converter;
import cc.chordflower.structuredweaver.annotations.binary.Field;
import cc.chordflower.structuredweaver.annotations.common.FieldOrder;
import cc.chordflower.structuredweaver.utils.BinaryTypes;

@BinaryModel
@FieldOrder({"magicNumber", "elfClass", "endiness", "version",
	"osABI", "abiVersion", "skip", "type", "machine", "version2", "entryPoint",
	"programHeaderOffset", "sectionHeaderOffset", "flags", "size",
	"programHeaderSize", "programHeaderEntries", "sectionHeaderSize",
	"sectionHeaderEntries", "sectionHeaderNamesIndex"})
public class ELFHeader {

	@Field( type=BinaryTypes.UINT )
	@Converter( value=MagicNumberConverter.class )
	private String magicNumber;

	//@Field( type=BinaryTypes.UBYTE )
	private Byte elfClass;

	//@Field( type=BinaryTypes.UBYTE )
	private Byte endiness;

	//@Field( type=BinaryTypes.UBYTE )
	private Byte version;

	//@Field( type=BinaryTypes.UBYTE )
	private Byte osABI;

	//@Field( type=BinaryTypes.UBYTE )
	private Byte abiVersion;

	//@Field( type=BinaryTypes.USHORT )
	private Short type;

	//@Field( type=BinaryTypes.USHORT )
	private Short machine;

	//@Field( type=BinaryTypes.UINT )
	private Integer version2;

	//@Field( type=BinaryTypes.ULONG )
	private Long entryPoint;

	//@Field( type=BinaryTypes.ULONG )
	private Long programHeaderOffset;

	//@Field( type=BinaryTypes.ULONG )
	private Long sectionHeaderOffset;

	//@Field( type=BinaryTypes.UINT )
	private Integer flags;

	//@Field( type=BinaryTypes.USHORT )
	private Short size;

	//@Field( type=BinaryTypes.USHORT )
	private Short programHeaderSize;

	//@Field( type=BinaryTypes.USHORT )
	private Short programHeaderEntries;

	//@Field( type=BinaryTypes.USHORT )
	private Short sectionHeaderSize;

	//@Field( type=BinaryTypes.USHORT )
	private Short sectionHeaderEntries;

	//@Field( type=BinaryTypes.USHORT )
	private Short sectionHeaderNamesIndex;

  public ELFHeader() {

	}

	public ELFHeader(String magicNumber, Byte elfClass,
		Byte endiness, Byte version, Byte osABI, Byte abiVersion, Short type,
		Short machine, Integer version2, Long entryPoint, Long programHeaderOffset,
		Long sectionHeaderOffset, Integer flags, Short size, Short programHeaderSize,
		Short programHeaderEntries, Short sectionHeaderSize, Short sectionHeaderEntries,
		Short sectionHeaderNamesIndex) {

		this.magicNumber = magicNumber;
		this.elfClass = elfClass;
		this.endiness = endiness;
		this.version = version;
		this.osABI = osABI;
		this.abiVersion = abiVersion;
		this.type = type;
		this.machine = machine;
		this.version2 = version2;
		this.entryPoint = entryPoint;
		this.programHeaderOffset = programHeaderOffset;
		this.sectionHeaderOffset = sectionHeaderOffset;
		this.flags = flags;
		this.size = size;
		this.programHeaderSize = programHeaderSize;
		this.programHeaderEntries = programHeaderEntries;
		this.sectionHeaderSize = sectionHeaderSize;
		this.sectionHeaderEntries = sectionHeaderEntries;
		this.sectionHeaderNamesIndex = sectionHeaderNamesIndex;
	}

	public String getMagicNumber() {
		return this.magicNumber;
	}

	public void setMagicNumber(String magicNumber) {
		this.magicNumber = magicNumber;
	}

	public Byte getElfClass() {
		return this.elfClass;
	}

	public void setElfClass(Byte elfClass) {
		this.elfClass = elfClass;
	}

	public Byte getEndiness() {
		return this.endiness;
	}

	public void setEndiness(Byte endiness) {
		this.endiness = endiness;
	}

	public Byte getVersion() {
		return this.version;
	}

	public void setVersion(Byte version) {
		this.version = version;
	}

	public Byte getOsABI() {
		return this.osABI;
	}

	public void setOsABI(Byte osABI) {
		this.osABI = osABI;
	}

	public Byte getAbiVersion() {
		return this.abiVersion;
	}

	public void setAbiVersion(Byte abiVersion) {
		this.abiVersion = abiVersion;
	}

	public Short getType() {
		return this.type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public Short getMachine() {
		return this.machine;
	}

	public void setMachine(Short machine) {
		this.machine = machine;
	}

	public Integer getVersion2() {
		return this.version2;
	}

	public void setVersion2(Integer version2) {
		this.version2 = version2;
	}

	public Long getEntryPoint() {
		return this.entryPoint;
	}

	public void setEntryPoint(Long entryPoint) {
		this.entryPoint = entryPoint;
	}

	public Long getProgramHeaderOffset() {
		return this.programHeaderOffset;
	}

	public void setProgramHeaderOffset(Long programHeaderOffset) {
		this.programHeaderOffset = programHeaderOffset;
	}

	public Long getSectionHeaderOffset() {
		return this.sectionHeaderOffset;
	}

	public void setSectionHeaderOffset(Long sectionHeaderOffset) {
		this.sectionHeaderOffset = sectionHeaderOffset;
	}

	public Integer getFlags() {
		return this.flags;
	}

	public void setFlags(Integer flags) {
		this.flags = flags;
	}

	public Short getSize() {
		return this.size;
	}

	public void setSize(Short size) {
		this.size = size;
	}

	public Short getProgramHeaderSize() {
		return this.programHeaderSize;
	}

	public void setProgramHeaderSize(Short programHeaderSize) {
		this.programHeaderSize = programHeaderSize;
	}

	public Short getProgramHeaderEntries() {
		return this.programHeaderEntries;
	}

	public void setProgramHeaderEntries(Short programHeaderEntries) {
		this.programHeaderEntries = programHeaderEntries;
	}

	public Short getSectionHeaderSize() {
		return this.sectionHeaderSize;
	}

	public void setSectionHeaderSize(Short sectionHeaderSize) {
		this.sectionHeaderSize = sectionHeaderSize;
	}

	public Short getSectionHeaderEntries() {
		return this.sectionHeaderEntries;
	}

	public void setSectionHeaderEntries(Short sectionHeaderEntries) {
		this.sectionHeaderEntries = sectionHeaderEntries;
	}

	public Short getSectionHeaderNamesIndex() {
		return this.sectionHeaderNamesIndex;
	}

	public void setSectionHeaderNamesIndex(Short sectionHeaderNamesIndex) {
		this.sectionHeaderNamesIndex = sectionHeaderNamesIndex;
	}

}
