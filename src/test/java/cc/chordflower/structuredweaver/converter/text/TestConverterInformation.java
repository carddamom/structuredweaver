/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.text;

public final class TestConverterInformation<T> {

    private final TextConverter<T> converter;

    private final T testSubject;

    private final char[] expectedResult;

    public TestConverterInformation(TextConverter<T> converter, T testSubject, char[] expectedResult) {
        this.converter = converter;
        this.testSubject = testSubject;
        this.expectedResult = expectedResult;
    }

    public TextConverter<T> getConverter() {
        return this.converter;
    }

    public T getTestSubject() {
        return this.testSubject;
    }

    public char[] getExpectedResult() {
        return this.expectedResult;
    }
}
