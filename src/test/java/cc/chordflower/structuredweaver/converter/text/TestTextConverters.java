/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.text;

import static com.mscharhag.oleaster.matcher.Matchers.expect;
import static j8spec.J8Spec.describe;
import static j8spec.J8Spec.it;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.runner.RunWith;

import cc.chordflower.structuredweaver.converter.text.basic.BooleanConverter;
import cc.chordflower.structuredweaver.converter.text.basic.ByteConverter;
import cc.chordflower.structuredweaver.converter.text.basic.CharacterConverter;
import cc.chordflower.structuredweaver.converter.text.basic.DoubleConverter;
import cc.chordflower.structuredweaver.converter.text.basic.FloatConverter;
import cc.chordflower.structuredweaver.converter.text.basic.IntegerConverter;
import cc.chordflower.structuredweaver.converter.text.basic.LongConverter;
import cc.chordflower.structuredweaver.converter.text.basic.ShortConverter;
import cc.chordflower.structuredweaver.converter.text.basic.StringConverter;
import cc.chordflower.structuredweaver.converter.text.concurrent.AtomicBooleanConverter;
import cc.chordflower.structuredweaver.converter.text.concurrent.AtomicIntegerConverter;
import cc.chordflower.structuredweaver.converter.text.concurrent.AtomicLongConverter;
import j8spec.junit.J8SpecRunner;

@RunWith(J8SpecRunner.class)
public final class TestTextConverters {

    {

        describe("Given a TextConverter", () -> {

            final List<TestConverterInformation> testConverterInformation = new ArrayList<>();

            testConverterInformation.add(new TestConverterInformation(new AtomicIntegerConverter(), new AtomicInteger(1), "1".toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new StringConverter(), "AAA", "AAA".toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new ShortConverter(), Short.valueOf((short) 2), "2".toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new LongConverter(), Long.valueOf(2l), "2".toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new IntegerConverter(), 1, "1".toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new FloatConverter(), 1.23f, "1.23".toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new DoubleConverter(), 1.2343, "1.2343".toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new CharacterConverter(), 'A', "A".toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new ByteConverter(), Byte.valueOf((byte) 2), "2".toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new BooleanConverter(), Boolean.TRUE, Boolean.TRUE.toString().toCharArray()));
            testConverterInformation
                    .add(new TestConverterInformation(new AtomicBooleanConverter(), new AtomicBoolean(Boolean.FALSE), Boolean.FALSE.toString().toCharArray()));
            testConverterInformation.add(new TestConverterInformation(new AtomicLongConverter(), new AtomicLong(20L), "20".toCharArray()));

            it("it should be able to convert a object to a char array", () -> {
                testConverterInformation.forEach((information) -> {
                    expect(Arrays.toString(information.getConverter().convertFrom(information.getTestSubject())))
                            .toEqual(Arrays.toString(information.getExpectedResult()));
                });
            });

            it("it should be able to convert a char array to a object", () -> {
                testConverterInformation.forEach((information) -> {
                    expect(information.getConverter().convertTo(information.getExpectedResult()).toString()).toEqual(information.getTestSubject().toString());
                });
            });

            it("it should be able to convert a object to a char array of the expected size", () -> {
                testConverterInformation.forEach((information) -> {
                    if (information.getConverter().atLeast() != null && information.getConverter().atMost() != null) {
                        expect(information.getConverter().convertFrom(information.getTestSubject()).length).toBeBetween(information.getConverter().atLeast() - 1,
                                information.getConverter().atMost() + 1);
                    }
                    if (information.getConverter().atLeast() == null && information.getConverter().atMost() != null) {
                        expect(information.getConverter().convertFrom(information.getTestSubject()).length).toBeSmallerThan(information.getConverter().atMost() + 1);
                    }
                    if (information.getConverter().atLeast() != null && information.getConverter().atMost() == null) {
                        expect(information.getConverter().convertFrom(information.getTestSubject()).length).toBeGreaterThan(information.getConverter().atLeast() - 1);
                    }
                });
            });
        });
    }
}
