/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.utils;

/**
 * Specifies the possible binary types for a field.
 */
public enum BinaryTypes {

	/**
	 * Specifies an type with 1 byte, signed and encoded in big endian.
	 */
	BYTE_BE(1,true,true),
	/**
	 * Specifies an type with 1 byte, signed and encoded in little endian.
	 */
	BYTE_LE(1,true,false),
	/**
	 * Specifies an type with 1 byte and signed.
	 */
	BYTE(1,true,null),
	/**
	 * Specifies an type with 1 byte, unsigned and encoded in big endian.
	 */
	UBYTE_BE(1,false,true),
	/**
	 * Specifies an type with 1 byte, unsigned and encoded in little endian.
	 */
	UBYTE_LE(1,false,false),
	/**
	 * Specifies an type with 1 byte and unsigned
	 */
	UBYTE(1,false,null),
	/**
	 * Specifies an type with 2 bytes, signed and encoded in big endian.
	 */
	SHORT_BE(2,true,true),
	/**
	 * Specifies an type with 2 bytes, signed and encoded in little endian.
	 */
	SHORT_LE(2,true,false),
	/**
	 * Specifies an type with 2 bytes and signed.
	 */
	SHORT(2,true,null),
	/**
	 * Specifies an type with 2 bytes, unsigned and encoded in big endian.
	 */
	USHORT_BE(2,false,true),
	/**
	 * Specifies an type with 2 bytes, unsigned and encoded in little endian.
	 */
	USHORT_LE(2,false,false),
	/**
	 * Specifies an type with 2 bytes and unsigned.
	 */
	USHORT(2,false,null),
	/**
	 * Specifies an type with 4 bytes, signed and encoded in big endian.
	 */
	INT_BE(4,true,true),
	/**
	 * Specifies an type with 4 bytes, signed and encoded in little endian.
	 */
	INT_LE(4,true,false),
	/**
	 * Specifies an type with 4 bytes and signed.
	 */
	INT(4,true,null),
	/**
	 * Specifies an type with 4 bytes, unsigned and encoded in big endian.
	 */
	UINT_BE(4,false,true),
	/**
	 * Specifies an type with 4 bytes, unsigned and encoded in little endian.
	 */
	UINT_LE(4,false,false),
	/**
	 * Specifies an type with 4 bytes and unsigned.
	 */
	UINT(4,false,null),
	/**
	 * Specifies an type with 8 bytes, signed and encoded in big endian.
	 */
	LONG_BE(8,true,true),
	/**
	 * Specifies an type with 8 bytes, signed and encoded in little endian.
	 */
	LONG_LE(8,true,false),
	/**
	 * Specifies an type with 8 bytes and signed.
	 */
	LONG(8,true,null),
	/**
	 * Specifies an type with 8 bytes, unsigned and encoded in big endian.
	 */
	ULONG_BE(8,false,true),
	/**
	 * Specifies an type with 8 bytes, unsigned and encoded in little endian.
	 */
	ULONG_LE(8,false,false),
	/**
	 * Specifies an type with 8 bytes and unsigned.
	 */
	ULONG(8,false,null);

	private final int size;

	private final boolean signed;

	private final Boolean bigEndianess;

  private BinaryTypes(int size, boolean signed, Boolean bigEndian) {
		this.size = size;
		this.signed = signed;
		this.bigEndianess = bigEndian;
	}

	/**
	 * Returns the size of this type.
	 *
	 * @return The size of this type in bytes.
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Checks if this type is signed or not.
	 *
	 * @return True if this type is signed, false if unsigned.
	 */
	public boolean isSigned() {
		return signed;
	}

	/**
	 * Checks if this type is big endian, little endian or platform dependant.
	 *
	 * @return Boolean.True if big endian, Boolean.False if little endian or null
	 *  if platform dependant (aka don't care).
	 */
	public Boolean getBigEndianess() {
		return bigEndianess;
	}

}
