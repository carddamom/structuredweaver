/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.binary.concurrent;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import cc.chordflower.structuredweaver.converter.binary.BinaryConverter;
import cc.chordflower.structuredweaver.utils.BinaryTypes;
import com.google.auto.service.AutoService;

@AutoService(BinaryConverter.class)
public class AtomicBooleanConverter implements BinaryConverter<AtomicBoolean> {

	public void parameters(List<String> parameters) { }

	public AtomicBoolean convertTo(byte[] input) {
		return new AtomicBoolean( ByteBuffer.wrap(input).get() == 1 ? Boolean.TRUE : Boolean.FALSE );
	}

	public byte[] convertFrom(AtomicBoolean input) {
		return ByteBuffer.allocate(requires()).put( (byte) (Boolean.TRUE.equals(input.get()) ? 1 : 0) ).array();
	}

	public Integer requires() {
		return BinaryTypes.BYTE.getSize();
	}

}
