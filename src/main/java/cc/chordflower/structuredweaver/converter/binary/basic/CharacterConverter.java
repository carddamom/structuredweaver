/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.binary.basic;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.List;
import cc.chordflower.structuredweaver.converter.binary.BinaryConverter;
import com.google.auto.service.AutoService;

@AutoService(BinaryConverter.class)
public class CharacterConverter implements BinaryConverter<Character> {

	private Charset charset = Charset.forName("UTF-8");


	public void parameters(List<String> parameters) {
		if(!parameters.isEmpty()) {
			try {
				charset = Charset.forName(parameters.get(0));
			} catch( UnsupportedCharsetException ex ) {
				charset = Charset.forName("UTF-8");
			}
		}
	}


	public Character convertTo(byte[] input) {
		return charset.decode(ByteBuffer.wrap(input)).get();
	}


	public byte[] convertFrom(Character input) {
		return input.toString().getBytes(charset);
	}


	public Integer requires() {
		return null;
	}

}
