/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.binary.concurrent;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import cc.chordflower.structuredweaver.converter.binary.BinaryConverter;
import cc.chordflower.structuredweaver.utils.BinaryTypes;
import com.google.auto.service.AutoService;

@AutoService(BinaryConverter.class)
public class AtomicLongConverter implements BinaryConverter<AtomicLong> {

	public void parameters(List<String> parameters) { }

	public AtomicLong convertTo(byte[] input) {
		return new AtomicLong(ByteBuffer.wrap(input).getLong());
	}

	public byte[] convertFrom(AtomicLong input) {
		return ByteBuffer.allocate(requires()).putLong(input.get()).array();
	}

	public Integer requires() {
		return BinaryTypes.LONG.getSize();
	}

}
