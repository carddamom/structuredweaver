/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.binary.math;

import java.util.List;
import java.math.BigInteger;

import cc.chordflower.structuredweaver.converter.binary.BinaryConverter;
import com.google.auto.service.AutoService;

@AutoService(BinaryConverter.class)
public class BigIntegerConverter implements BinaryConverter<BigInteger> {


	public void parameters(List<String> parameters) { }


	public BigInteger convertTo(byte[] input) {
		return new BigInteger(input);
	}


	public byte[] convertFrom(BigInteger input) {
		return input.toByteArray();
	}


	public Integer requires() {
		return null;
	}

}
