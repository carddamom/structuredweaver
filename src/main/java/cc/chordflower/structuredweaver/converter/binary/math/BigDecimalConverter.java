/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.binary.math;

import java.util.List;
import java.math.BigDecimal;
import java.math.BigInteger;

import cc.chordflower.structuredweaver.converter.binary.BinaryConverter;
import com.google.auto.service.AutoService;

@AutoService(BinaryConverter.class)
public class BigDecimalConverter implements BinaryConverter<BigDecimal> {

	private int scale = 5;


	public void parameters(List<String> parameters) {
		if(!parameters.isEmpty()) {
			try {
				scale = Integer.parseInt(parameters.get(0));
			} catch( NumberFormatException ex ) {
				scale = 5;
			}
		}
	}


	public BigDecimal convertTo(byte[] input) {
		return new BigDecimal(new BigInteger(input), scale);
	}


	public byte[] convertFrom(BigDecimal input) {
		return input.unscaledValue().toByteArray();
	}


	public Integer requires() {
		return null;
	}

}
