/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.binary;

import cc.chordflower.structuredweaver.converter.Converter;

/**
 * Specified an converter to convert binary fields to the given type.
 */
public interface BinaryConverter<Type> extends Converter {

	/**
	 * Converts an sequence of bytes to an given type.
	 *
	 * @param input The sequence to convert.
	 * @return The result from the conversion.
	 */
	Type convertTo(byte[] input);

	/**
	 * Converts an type to a sequence of bytes.
	 *
	 * @param input The type to convert.
	 * @return The sequence of bytes representing the converted type.
	 */
	byte[] convertFrom(Type input);

	/**
	 * Specifies the number of bytes consumed or produced by this
	 * converter.
	 *
	 * @return The number of bytes consumed or produced by this
	 * converter, must be always bigger than zero.
	 */
	Integer requires();

}
