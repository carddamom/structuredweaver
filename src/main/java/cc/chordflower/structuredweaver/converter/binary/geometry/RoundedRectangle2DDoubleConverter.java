/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.binary.geometry;

import java.awt.geom.RoundRectangle2D;
import java.awt.geom.RoundRectangle2D.Double;
import java.util.List;
import cc.chordflower.structuredweaver.converter.binary.BinaryConverter;
import com.google.auto.service.AutoService;

/**
 * (double x, double y, double w, double h)
 */
@AutoService(BinaryConverter.class)
public class RoundedRectangle2DDoubleConverter implements BinaryConverter<RoundRectangle2D.Double> {


	public void parameters(List<String> parameters) {

	}


	public Double convertTo(byte[] input) {
		return null;
	}


	public byte[] convertFrom(Double input) {
		return null;
	}


	public Integer requires() {
		return null;
	}

}
