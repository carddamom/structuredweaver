/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.text.concurrent;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import cc.chordflower.structuredweaver.converter.text.TextConverter;
import com.google.auto.service.AutoService;

@AutoService(TextConverter.class)
public class AtomicBooleanConverter implements TextConverter<AtomicBoolean> {


	public void parameters(List<String> parameters) { }


	public AtomicBoolean convertTo(char[] input) {
		return new AtomicBoolean(Boolean.parseBoolean(String.valueOf(input)));
	}


	public char[] convertFrom(AtomicBoolean input) {
		return input.toString().toCharArray();
	}


	public Integer atLeast() {
		return Boolean.TRUE.toString().length();
	}


	public Integer atMost() {
		return Boolean.FALSE.toString().length();
	}

}
