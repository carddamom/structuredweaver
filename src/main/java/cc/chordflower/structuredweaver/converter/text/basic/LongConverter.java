/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.text.basic;

import java.util.List;
import cc.chordflower.structuredweaver.converter.text.TextConverter;
import com.google.auto.service.AutoService;

@AutoService(TextConverter.class)
public class LongConverter implements TextConverter<Long> {


	@Override
    public void parameters(List<String> parameters) { }


	@Override
    public Long convertTo(char[] input) {
		return Long.parseLong( String.valueOf(input) );
	}


	@Override
    public char[] convertFrom(Long input) {
		return input.toString().toCharArray();
	}


	@Override
    public Integer atLeast() {
		return 0;
	}


	@Override
    public Integer atMost() {
        return Long.toString(Long.MAX_VALUE).length();
	}

}
