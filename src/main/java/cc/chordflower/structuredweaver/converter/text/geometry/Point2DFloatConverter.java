/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.text.geometry;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;
import java.util.List;
import cc.chordflower.structuredweaver.converter.text.TextConverter;
import com.google.auto.service.AutoService;

/**
 * (float x, float y)
 */
@AutoService(TextConverter.class)
public class Point2DFloatConverter implements TextConverter<Point2D.Float> {


	public void parameters(List<String> parameters) {

	}


	public Float convertTo(char[] input) {
		return null;
	}


	public char[] convertFrom(Float input) {
		return null;
	}


	public Integer atLeast() {
		return null;
	}


	public Integer atMost() {
		return null;
	}

}
