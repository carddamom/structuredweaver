/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.text.basic;

import java.util.List;
import cc.chordflower.structuredweaver.converter.text.TextConverter;
import com.google.auto.service.AutoService;

@AutoService(TextConverter.class)
public class ByteConverter implements TextConverter<Byte> {


	@Override
    public void parameters(List<String> parameters) { }


	@Override
    public Byte convertTo(char[] input) {
		return Byte.parseByte( String.valueOf(input) );
	}


	@Override
    public char[] convertFrom(Byte input) {
		return input.toString().toCharArray();
	}


	@Override
    public Integer atLeast() {
        return 1;
	}


	@Override
    public Integer atMost() {
        return Byte.toString(Byte.MAX_VALUE).length();
	}

}
