/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.text.concurrent;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import cc.chordflower.structuredweaver.converter.text.TextConverter;
import com.google.auto.service.AutoService;

@AutoService(TextConverter.class)
public class AtomicLongConverter implements TextConverter<AtomicLong> {


	public void parameters(List<String> parameters) { }


	public AtomicLong convertTo(char[] input) {
		return new AtomicLong( Long.parseLong(String.valueOf(input)));
	}


	public char[] convertFrom(AtomicLong input) {
		return input.toString().toCharArray();
	}


	public Integer atLeast() {
		return 1;
	}


	public Integer atMost() {
		return Long.toString(Long.MAX_VALUE).length();
	}

}
