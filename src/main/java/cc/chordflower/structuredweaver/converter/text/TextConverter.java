/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.converter.text;

import cc.chordflower.structuredweaver.converter.Converter;

/**
 * Specifies a converter that converts a given type to and from a sequence
 * of characters.
 */
public interface TextConverter<Type> extends Converter {

	/**
	 * Converts an sequence of characters to an given type.
	 *
	 * @param input The sequence to convert.
	 * @return The result from the conversion.
	 */
	Type convertTo(char[] input);

	/**
	 * Converts an type to a sequence of characters.
	 *
	 * @param input The type to convert.
	 * @return The sequence of characters representing the converted type.
	 */
	char[] convertFrom(Type input);

	/**
	 * Specifies the minimum number of characters that this converter consumes.
	 *
	 * @return The minimum number of characters that this converter consumes (can
	 *  be null).
	 */
	Integer atLeast();

	/**
	 * Specifies the maximum number of characters that this converter consumes.
	 *
	 * @return The maximum number of characters that this converter consumes (can
	 *  be null).
	 */
	Integer atMost();

}
