/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.io;

import cc.chordflower.structuredweaver.annotations.text.Converter;
import cc.chordflower.structuredweaver.annotations.text.Field;
import cc.chordflower.structuredweaver.exceptions.BaseStructuredWeaverException;
import jodd.introspector.ClassDescriptor;
import jodd.introspector.MethodDescriptor;
import jodd.util.StringUtil;

public abstract class AbstractStructureIO {

	protected final String SKIP_FIELD = ":skip";

	protected boolean hasGetterMethod(ClassDescriptor destiny, String field) {
		return destiny.getMethodDescriptor( "get" + StringUtil.capitalize(field), true) != null;
	}

	protected MethodDescriptor getGetterMethod(ClassDescriptor destiny, String field) {
		return destiny.getMethodDescriptor( "get" + StringUtil.capitalize(field), true);
	}

	protected boolean hasSetterMethod(ClassDescriptor destiny, String field) {
		return destiny.getAllMethodDescriptors( "set" + StringUtil.capitalize(field) ).length > 0;
	}

	protected MethodDescriptor getSetterMethod(ClassDescriptor destiny, String field) {
		return this.hasSetterMethod(destiny, field) ? destiny.getAllMethodDescriptors( "set" + StringUtil.capitalize(field) )[0] : null;
	}
}
