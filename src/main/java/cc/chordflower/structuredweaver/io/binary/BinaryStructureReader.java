/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.io.binary;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import cc.chordflower.structuredweaver.annotations.binary.BinaryModel;
import cc.chordflower.structuredweaver.annotations.common.FieldOrder;
import cc.chordflower.structuredweaver.converter.binary.BinaryConverter;
import cc.chordflower.structuredweaver.exceptions.BaseStructuredWeaverException;
import cc.chordflower.structuredweaver.exceptions.BinaryReadWeaverException;
import jodd.bean.BeanUtil;
import jodd.introspector.ClassDescriptor;
import jodd.introspector.ClassIntrospector;

@SuppressWarnings({"rawtypes", "unchecked"})
public final class BinaryStructureReader extends AbstractBinaryStructureIO {

	public BinaryStructureReader() {
		super();
	}

	public <T> T readFrom(ByteBuffer buffer, Class<T> destiny) throws BinaryReadWeaverException {

		if (buffer == null || destiny == null) {
			throw new BinaryReadWeaverException("The input is null");
		}

		T result;
		try {
			result = destiny.newInstance();
		}
		catch (InstantiationException | IllegalAccessException ex) {
			throw new BinaryReadWeaverException("The destiny class does not have a parameterless constructor.", ex);
		}

		if (destiny.getAnnotation(BinaryModel.class) != null && destiny.getAnnotation(FieldOrder.class) != null) {
			FieldOrder fieldOrder = destiny.getAnnotation(FieldOrder.class);
			for (String field : fieldOrder.value()) {
				readField(buffer, ClassIntrospector.get().lookup(destiny), result, field);
			}
		}
		else {
			throw new BinaryReadWeaverException("The destiny class is invalid.");
		}
		return result;
	}

	private <T> void readField(ByteBuffer buffer, ClassDescriptor destiny, T result, String field)
					throws BinaryReadWeaverException {
		if (field.startsWith(SKIP_FIELD)) {
			Integer offset = 1;
			if (!field.endsWith(SKIP_FIELD)) {
				offset = Integer.parseInt(field.replace(":skip ", ""));
			}
			for (int i = 0; i < offset; i++) {
				buffer.get();
			}
		}
		else if (BeanUtil.pojo.hasProperty(result, field)) {
			readTypedField(buffer, destiny, result, field);
		}  else {
			throw new BinaryReadWeaverException("The destiny class is invalid. It does not have property " + field);
		}
	}

	private <T> void readTypedField(ByteBuffer buffer, ClassDescriptor destiny, T result, String field)
					throws BinaryReadWeaverException {
		FieldInformation fieldInformation = null;
		try {
			fieldInformation = this.getFieldInformation(destiny, field);
		}
		catch (BaseStructuredWeaverException ex) {
			throw new BinaryReadWeaverException("The field named " + field + " does not exist in the given source object.", ex);
		}
		byte[] value;
		try {
			if (fieldInformation.currentField != null) {
				value = readBytes(buffer, fieldInformation.currentField.type().getSize() * fieldInformation.currentField.size());
				value = dealWithEndiness(fieldInformation, value);

				BinaryConverter converter = findConverterForField(fieldInformation.fieldConverter, fieldInformation.resultType);
				setFieldValue(result, field, fieldInformation.resultType, value, converter);
			}
		}
		catch (SecurityException | IllegalArgumentException | BaseStructuredWeaverException ex) {
			throw new BinaryReadWeaverException(ex);
		}
	}

	private <T> void setFieldValue(T result, String field, Class resultType, byte[] value,
					BinaryConverter converter) throws BinaryReadWeaverException {
		Object fieldValue = (converter != null) ? converter.convertTo(value) : this.readFrom(ByteBuffer.wrap(value), resultType);
		BeanUtil.pojo.setProperty(result, field, fieldValue);
	}

	private byte[] readBytes(ByteBuffer buffer, int size) {
		byte[] value = new byte[size];
		buffer.get(value, 0, size);
		return value;
	}

}
