/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.io.binary;

import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import cc.chordflower.structuredweaver.annotations.binary.BinaryModel;
import cc.chordflower.structuredweaver.annotations.common.FieldOrder;
import cc.chordflower.structuredweaver.converter.binary.BinaryConverter;
import cc.chordflower.structuredweaver.exceptions.BaseStructuredWeaverException;
import cc.chordflower.structuredweaver.exceptions.BinaryWriterWeaverException;
import jodd.bean.BeanUtil;
import jodd.introspector.ClassDescriptor;
import jodd.introspector.ClassIntrospector;

@SuppressWarnings({"rawtypes", "unchecked"})
public final class BinaryStructureWriter extends AbstractBinaryStructureIO {

	public BinaryStructureWriter() {
		super();
	}

	public <T> void writeInto(ByteBuffer buffer, T source, Class<T> sourceClass) throws BinaryWriterWeaverException {
		if (buffer == null || source == null) {
			throw new BinaryWriterWeaverException("The parameters are null");
		}
		if (sourceClass.getAnnotation(BinaryModel.class) != null && sourceClass.getAnnotation(FieldOrder.class) != null) {
			for (String field : sourceClass.getAnnotation(FieldOrder.class).value()) {
				writeField(buffer, ClassIntrospector.get().lookup(sourceClass), source, field);
			}
		}
		else {
			throw new BinaryWriterWeaverException("The destiny class does not have the required"
							+ " FieldOrder annotation.");
		}
	}

	private <T> void writeField(ByteBuffer buffer, ClassDescriptor sourceClass, T source, String field)
					throws BinaryWriterWeaverException {
		if (field.startsWith(SKIP_FIELD)) {
			Integer offset = 1;
			if (!field.endsWith(SKIP_FIELD)) {
				offset = Integer.parseInt(field.replace(":skip ", ""));
			}
			for (int i = 0; i < offset; i++) {
				buffer.put((byte) 0x00);
			}
		}
		else if (BeanUtil.pojo.hasProperty(source, field)) {
			writeTypedField(buffer, sourceClass, source, field);
		}
	}

	private <T> void writeTypedField(ByteBuffer buffer, ClassDescriptor sourceClass, T source, String field)
					throws BinaryWriterWeaverException {
		AbstractBinaryStructureIO.FieldInformation fieldInformation = null;
		try {
			fieldInformation = this.getFieldInformation(sourceClass, field);
		}
		catch (BaseStructuredWeaverException ex) {
			throw new BinaryWriterWeaverException("The field named " + field + " does not exist in the given source object.", ex);
		}

		try {
			writeValue(buffer, source, field, fieldInformation);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | BaseStructuredWeaverException | NoSuchFieldException
						| SecurityException ex) {
			throw new BinaryWriterWeaverException(ex);
		}
	}

	private <T> void writeValue(ByteBuffer buffer, T source, String field, FieldInformation fieldInformation) throws IllegalAccessException, InvocationTargetException, BaseStructuredWeaverException, NoSuchFieldException {
		Object result = BeanUtil.pojo.getProperty(source, field);
		BinaryConverter converter = findConverterForField(fieldInformation.fieldConverter, fieldInformation.resultType);

		if (converter != null) {
			byte[] value = converter.convertFrom(result);
			value = dealWithEndiness(fieldInformation, value);

			if (!Integer.valueOf(value.length).equals(converter.requires())) {
				throw new BinaryWriterWeaverException("The result of the conversion from the converter is different than the declared one, at" + field);
			}

			if (!Integer.valueOf(value.length).equals(fieldInformation.currentField.type().getSize() * fieldInformation.currentField.size())) {
				throw new BinaryWriterWeaverException(
								"The result of the conversion from the converter is different than the one declared in the Field annotation, at" + field);
			}
			buffer.put(value);
		}
		else {
			this.writeInto(buffer, result, fieldInformation.resultType);
		}
	}
}
