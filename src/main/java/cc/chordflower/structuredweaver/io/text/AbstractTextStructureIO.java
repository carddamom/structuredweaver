/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.io.text;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import cc.chordflower.structuredweaver.annotations.text.Converter;
import cc.chordflower.structuredweaver.annotations.text.Field;
import cc.chordflower.structuredweaver.converter.text.TextConverter;
import cc.chordflower.structuredweaver.exceptions.BaseStructuredWeaverException;
import cc.chordflower.structuredweaver.io.AbstractStructureIO;
import java.util.ServiceLoader;
import jodd.introspector.ClassDescriptor;

public class AbstractTextStructureIO extends AbstractStructureIO {

	protected Map<Class, TextConverter> converterMap = new ConcurrentSkipListMap<>();

	protected AbstractTextStructureIO() {
		ServiceLoader.load(TextConverter.class).forEach((TextConverter converter) -> {
			try {
				converterMap.putIfAbsent(
								converter.getClass().getDeclaredMethod("convertTo", byte[].class).getGenericReturnType().getClass(),
								converter);
			}
			catch (NoSuchMethodException | SecurityException ex) {
			}
		});
	}

	public void addConverter(TextConverter converter) throws NoSuchMethodException {
		this.converterMap.put(converter.getClass().getDeclaredMethod("convertTo", byte[].class).getGenericReturnType().getClass(),
						converter);
	}

	public void addConverter(Class converterType, TextConverter converter) {
		this.converterMap.put(converterType, converter);
	}

	public boolean hasRegisteredConverterFor(Class converterType) {
		return this.converterMap.containsKey(converterType);
	}

	protected TextConverter findConverterForField(Converter fieldConverter, Class resultType) throws BaseStructuredWeaverException {
		try {
			TextConverter converter;
			if (fieldConverter != null) {
				converter = fieldConverter.value().newInstance();
				converter.parameters(Arrays.asList(fieldConverter.parameters()));
			}
			else {
				converter = this.converterMap.get(resultType);
			}
			return converter;
		}
		catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException ex) {
			throw new BaseStructuredWeaverException(ex);
		}
	}
	
	protected <T> FieldInformation getFieldInformation( ClassDescriptor destiny, String field ) throws BaseStructuredWeaverException {
		FieldInformation result = new FieldInformation();
		if (destiny.getFieldDescriptor(field, true) != null) {
			result.currentField = destiny.getFieldDescriptor(field, true).getField().getAnnotation(Field.class);
			result.fieldConverter = destiny.getFieldDescriptor(field, true).getField().getAnnotation(Converter.class);
			result.resultType = destiny.getFieldDescriptor(field, true).getField().getType();
		} else if (this.hasGetterMethod(destiny, field)) {
				result.currentField = this.getGetterMethod(destiny, field).getMethod().getAnnotation(Field.class);
				result.fieldConverter = this.getGetterMethod(destiny, field).getMethod().getAnnotation(Converter.class);
				result.resultType = this.getGetterMethod(destiny, field).getMethod().getReturnType();
		}
		return result;
	}
	
	protected class FieldInformation {
		public Field currentField = null;
		public Converter fieldConverter = null;
		public Class resultType = null;
	}
}
