/**
 *   Copyright 2018 carddamom
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package cc.chordflower.structuredweaver.io.text;

import java.nio.CharBuffer;

import cc.chordflower.structuredweaver.annotations.common.FieldOrder;
import cc.chordflower.structuredweaver.annotations.text.TextModel;
import cc.chordflower.structuredweaver.converter.text.TextConverter;
import cc.chordflower.structuredweaver.exceptions.BaseStructuredWeaverException;
import cc.chordflower.structuredweaver.exceptions.TextReadWeaverException;
import jodd.bean.BeanUtil;
import jodd.introspector.ClassDescriptor;
import jodd.introspector.ClassIntrospector;

@SuppressWarnings({"rawtypes", "unchecked"})
public final class TextStructureReader extends AbstractTextStructureIO {

	public TextStructureReader() {
		super();
	}

	public <T> T readFrom(CharBuffer buffer, Class<T> destiny) throws TextReadWeaverException {
		if (buffer == null || destiny == null) {
			throw new TextReadWeaverException("The input is null");
		}

		T result;
		try {
			result = destiny.newInstance();
		}
		catch (InstantiationException | IllegalAccessException ex) {
			throw new TextReadWeaverException("The destiny class does not have a parameterless constructor.", ex);
		}

		if (destiny.getAnnotation(TextModel.class) != null && destiny.getAnnotation(FieldOrder.class) != null ) {
			FieldOrder fieldOrder = destiny.getAnnotation(FieldOrder.class);
			for (String field : fieldOrder.value()) {
				readField(buffer, ClassIntrospector.get().lookup(destiny), result, field);
			}
		} else {
			throw new TextReadWeaverException("The destiny class is invalid.");
		}
		return result;
	}

	private <T> void readField(CharBuffer buffer, ClassDescriptor destiny, T result, String field)
					throws TextReadWeaverException {
		if (field.startsWith(SKIP_FIELD)) {
			Integer offset = 1;
			if (!field.endsWith(SKIP_FIELD)) {
				offset = Integer.parseInt(field.replace(":skip ", ""));
			}
			for (int i = 0; i < offset; i++) {
				buffer.get();
			}
		}
		else if (BeanUtil.pojo.hasProperty(result, field)) {
			readTypedField(buffer, destiny, result, field);
		} else {
			throw new TextReadWeaverException("The destiny class is invalid. It does not have property " + field);
		}
	}

	private <T> void readTypedField(CharBuffer buffer, ClassDescriptor destiny, T result, String field)
					throws TextReadWeaverException {
		FieldInformation fieldInformation = null;
		try {
			fieldInformation = this.getFieldInformation(destiny, field);
		}
		catch (BaseStructuredWeaverException ex) {
			throw new TextReadWeaverException("The field named " + field + " does not exist in the given source object.", ex);
		}
		char[] value;
		try {
			if (fieldInformation.currentField != null) {
				value = readCharacters(buffer, fieldInformation.currentField.size());
				TextConverter converter = findConverterForField(fieldInformation.fieldConverter, fieldInformation.resultType);
				setFieldValue(result, field, fieldInformation.resultType, value, converter);
			}
		}
		catch (SecurityException | IllegalArgumentException | BaseStructuredWeaverException ex) {
			throw new TextReadWeaverException(ex);
		}
	}

	private <T> void setFieldValue(T result, String field, Class resultType, char[] value,
					TextConverter converter) throws TextReadWeaverException {
		Object fieldValue = (converter != null) ? converter.convertTo(value) : this.readFrom(CharBuffer.wrap(value), resultType);
		BeanUtil.pojo.setProperty(result, field, fieldValue);
	}

	private char[] readCharacters(CharBuffer buffer, int size) {
		char[] value = new char[size];
		buffer.get(value, 0, size);
		return value;
	}
}
