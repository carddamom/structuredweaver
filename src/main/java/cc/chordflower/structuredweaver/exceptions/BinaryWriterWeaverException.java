package cc.chordflower.structuredweaver.exceptions;

public final class BinaryWriterWeaverException extends BaseStructuredWeaverException {

    private static final long serialVersionUID = -3887024654523761366L;

    public BinaryWriterWeaverException() {
    }

    public BinaryWriterWeaverException(String message) {
        super(message);
    }

    public BinaryWriterWeaverException(Throwable cause) {
        super(cause);
    }

    public BinaryWriterWeaverException(String message, Throwable cause) {
        super(message, cause);
    }

}
