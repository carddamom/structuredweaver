/*
 * Copyright 2017 by Brisa Inovação e Tecnologia S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Brisa, SA ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Brisa.
 */
package cc.chordflower.structuredweaver.exceptions;

public class TextWriterWeaverException extends BaseStructuredWeaverException {

    private static final long serialVersionUID = 5916186381484725345L;

    public TextWriterWeaverException() {
    }

    public TextWriterWeaverException(String message) {
        super(message);
    }

    public TextWriterWeaverException(Throwable cause) {
        super(cause);
    }

    public TextWriterWeaverException(String message, Throwable cause) {
        super(message, cause);
    }

}
