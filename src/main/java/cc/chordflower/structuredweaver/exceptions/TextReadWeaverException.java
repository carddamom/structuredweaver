package cc.chordflower.structuredweaver.exceptions;

public class TextReadWeaverException extends BaseStructuredWeaverException {

    private static final long serialVersionUID = 3518470438017696553L;

    public TextReadWeaverException() {
    }

    public TextReadWeaverException(String message) {
        super(message);
    }

    public TextReadWeaverException(Throwable cause) {
        super(cause);
    }

    public TextReadWeaverException(String message, Throwable cause) {
        super(message, cause);
    }

}
