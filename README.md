[![Maintainability](https://api.codeclimate.com/v1/badges/1e853a110fcae3cf8aa3/maintainability)](https://codeclimate.com/github/chordflower/structuredweaver/maintainability)[![codecov](https://codecov.io/gh/chordflower/structuredweaver/branch/master/graph/badge.svg)](https://codecov.io/gh/chordflower/structuredweaver)[![Build Status](https://travis-ci.org/chordflower/structuredweaver.svg?branch=master)](https://travis-ci.org/chordflower/structuredweaver)

# README

An reader/writer for structured binary or text files.
